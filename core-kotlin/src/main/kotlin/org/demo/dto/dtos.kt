package org.demo.dto

data class KotlinDto(
    val id: Long,
    val name: String,
    var content: String
)

data class KotlinDtoWithDefault(
    val id: Long,
    val name: String = "no name",
    var content: String
)

data class KotlinDtoWithNull(
    val id: Long,
    val name: String,
    var content: String?
)

data class CallDto(
    val id: Long,
    val totalDayCalls: Int,
    val totalEveCalls: Int,
    val totalNightCalls: Int,
    val totalCalls: Int
)

data class KotlinMessage<K, V>(
    val topic: String,
    val partition: Int? = null,
    val headers: Map<String, String> = mapOf(),
    val key: K? = null,
    val value: V
)
