package org.demo.serdes

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Serializer
import java.io.IOException

class JsonSerializer<T>(val objectMapper: ObjectMapper) : Serializer<T> {
    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) {
        // No-op
    }

    override fun serialize(topic: String?, data: T?): ByteArray? =
            try {
                data?.let {
                    objectMapper.writeValueAsBytes(it)
                }
            } catch (ex: IOException) {
                throw SerializationException("Can't serialize data [$data] for topic [$topic]", ex)
            }

    override fun close() {
        // No-op
    }
}