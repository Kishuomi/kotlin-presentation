package org.demo.serdes

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.serialization.Deserializer
import org.slf4j.LoggerFactory
import java.io.IOException

@Suppress("unchecked")
class JsonDeserializer<T>(val objectMapper: ObjectMapper, val clazz: Class<T>) : Deserializer<T> {

    protected val logger = LoggerFactory.getLogger(this::class.java)

    override fun configure(configs: MutableMap<String, *>?, isKey: Boolean) {
        // No-op
    }

    override fun deserialize(topic: String?, data: ByteArray?): T? =
            try {
                data?.let {
                    objectMapper.readValue(data, clazz) as T
                }
            } catch (ex: IOException) {
                logger.warn("Can't deserialize data from topic [$topic], skipping", ex)
                null
            }

    override fun close() {
        // No-op
    }
}