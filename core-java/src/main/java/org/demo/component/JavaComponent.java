package org.demo.component;

import org.demo.dto.JavaDto;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Objects;

public class JavaComponent {

    public String doubleString(String s) {
        return s + s;
    }

    public JavaDto makeDto(@Nullable Long id, String name, String content) {
        return Objects.nonNull(id) ? new JavaDto(id, name, content) : null;
    }

}
