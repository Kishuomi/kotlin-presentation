package org.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JavaDto {
    private final long id;
    private final String name;
    private String content;
}
