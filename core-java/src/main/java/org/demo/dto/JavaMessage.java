package org.demo.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JavaMessage<K, V> {
    private final String topic;
    private final Integer partition;
    private final Map<String, String> headers;
    private final K key;
    private final V value;

    public JavaMessage(@Nonnull final String topic, @Nonnull final V value) {
        this(topic, null, null, null, value);
    }

    public JavaMessage(@Nonnull final String topic, @Nullable final K key, @Nonnull final V value) {
        this(topic, null, null, key, value);
    }

    public JavaMessage(@Nonnull final String topic, @Nullable final Integer partition, @Nonnull final V value) {
        this(topic, partition, null, null, value);
    }

    public JavaMessage(
        @Nonnull final String topic,
        @Nullable Integer partition,
        @Nullable final K key,
        @Nonnull final V value) {
        this(topic, partition, null, key, value);
    }

    public JavaMessage(
        @Nonnull final String topic,
        @Nullable final Integer partition,
        @Nullable final Map<String, String> headers,
        @Nullable final K key,
        @Nonnull final V value
    ) {
        this.topic = topic;
        this.partition = partition;
        if (headers != null) {
            this.headers = headers;
        } else {
            this.headers = Collections.emptyMap();
        }
        this.key = key;
        this.value = value;
    }
}
