plugins {
    java
}

dependencies {
    annotationProcessor("org.projectlombok:lombok")
    implementation("org.projectlombok:lombok")
    implementation("com.google.code.findbugs:jsr305")
    implementation("org.apache.kafka:kafka-clients")
}