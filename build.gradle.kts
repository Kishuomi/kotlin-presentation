import java.net.URI

plugins {
    java
    kotlin("jvm") version "1.3.72"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
}

subprojects {
    apply {
        plugin("io.spring.dependency-management")
    }

    group = "org.demo"
    version = "1.0"

    repositories {
        mavenLocal()
        maven { url = URI("http://jcenter.bintray.com") }
        jcenter { url = URI("http://jcenter.bintray.com") }
        gradlePluginPortal()
    }

    dependencyManagement {
        dependencies {
            dependency("org.projectlombok:lombok:1.18.8")
            dependency("com.google.code.findbugs:jsr305:3.0.2")
            dependency("org.apache.kafka:kafka-clients:2.5.0")
            dependency("com.fasterxml.jackson.core:jackson-core:2.9.9")
            dependency("com.fasterxml.jackson.core:jackson-databind:2.9.9")
            dependency("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.9")
        }
    }

}

repositories {
    mavenLocal()
    maven { url = URI("http://jcenter.bintray.com") }
    jcenter { url = URI("http://jcenter.bintray.com") }
    gradlePluginPortal()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}


