package org.demo

import org.demo.dto.KotlinDto
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

/**
 * For showing diff between inline and no inline approach
 */

class ProbeInline {
    private val lock = ReentrantLock()
    private val map = mutableMapOf<Long, KotlinDto>()

    fun put(dto: KotlinDto) {
        lock(lock) { map.put(dto.id, dto) }
    }

    fun get(id: Long): KotlinDto? {
        lock(lock) {
            return map[id]
        }
    }
}

class Probe {
    private val lock = ReentrantLock()
    private val map = mutableMapOf<Long, KotlinDto>()

    fun put(dto: KotlinDto) {
        lockNoInline(lock) { map.put(dto.id, dto) }
    }

    fun get(id: Long): KotlinDto? {
        return lockNoInline(lock) {
            map[id]
        }
    }
}

private inline fun <T> lock(lock: Lock, body: () -> T): T {
    lock.lock()
    try {
        return body.invoke()
    } finally {
        lock.unlock()
    }
}

private fun <T> lockNoInline(lock: Lock, body: () -> T): T {
    lock.lock()
    try {
        return body.invoke()
    } finally {
        lock.unlock()
    }
}