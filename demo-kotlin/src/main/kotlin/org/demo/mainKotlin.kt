package org.demo

import org.demo.component.JavaComponent
import org.demo.dto.JavaDto
import org.demo.dto.KotlinDto
import org.demo.dto.KotlinMessage

fun main(args: Array<String>) {

    val kotlinMessage1 = KotlinMessage(
        "topic",
        1,
        mapOf(),
        "key",
        "value"
    )

    val kotlinMessage2 = KotlinMessage(
        topic = "topic",
        partition = 1,
        headers = mapOf(),
        key = "key",
        value = "value"
    )

    val kotlinMessage22 = KotlinMessage(
        topic = "topic",
        partition = 1,
        headers = mapOf(),
        key = "key",
        value = "value"
    )

    val kotlinMessage3 = KotlinMessage<String, String>(
        topic = "topic",
        value = "value"
    )


    println(kotlinMessage2) //tostring

    println(kotlinMessage2 == kotlinMessage22) // equal true

    println(kotlinMessage2 === kotlinMessage22) // false

























































    /*KotlinMessage2<Long, Long>(topic = "topic", value = 1L)

    val km = KotlinMessage2(topic = "d", key = "key", value = "value")
    println(km.partition.toString())
    val probe = ProbeInline()


    val dto = KotlinDto(1L, "dto name", "some content")
    probe.put(dto)
    val dto2 = probe.get(dto.id)
    val dto3 = KotlinDto(1L, "dto name", "some content")

    println(dto == dto2) //java equals
    println(dto === dto2) // java ==
    println(dto == dto3) //java equals
    println(dto === dto3) // java ==*/

}


private fun KotlinDto.copyWithNewContent(newContent: String): KotlinDto {
    return KotlinDto(id, name, newContent)
}

fun KotlinDto.makeDefault(id: Long?): KotlinDto? =
    if (id == null) {
        null
    } else {
        KotlinDto(id, "default", "default")
    }

fun JavaDto.copyWithNewContent(newContent: String): JavaDto {
    return JavaDto(id, name, newContent)
}

fun JavaComponent.makeDtoNonnull(id: Long?, name: String, content: String): JavaDto =
    if (id == null) {
        JavaDto(1L, "default", "default")
    } else {
        JavaDto(id, name, content)
    }


fun Any?.toString(): String {
    if (this == null) return "null is here"
    // after the null check, 'this' is autocast to a non-null type, so the toString() below
    // resolves to the member function of the Any class
    return toString()
}