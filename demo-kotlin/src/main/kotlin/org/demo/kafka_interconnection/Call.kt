package org.demo.kafka_interconnection

data class Call(
    val id: Long,
    val totalDayCalls: Int,
    val totalEveCalls: Int,
    val totalNightCalls: Int
)