package org.demo.kafka_interconnection

import org.demo.serdes.JsonSerializer
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.LongDeserializer
import org.apache.kafka.common.serialization.LongSerializer
import org.apache.kafka.common.serialization.StringDeserializer
import org.demo.dto.CallDto
import java.util.*

fun createProducer(): Producer<Long, CallDto> =
    Properties()
        .also {
            it[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
        }
        .let { KafkaProducer(it, LongSerializer(), JsonSerializer(createMapper())) }


fun createConsumer(groupId: String = "kotlin-presentation-group"): Consumer<Long, String> = Properties()
        .also {
            it[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:9092"
            it[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = LongDeserializer::class.java.name
            it[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java.name
            it[ConsumerConfig.GROUP_ID_CONFIG] = groupId
            it[ConsumerConfig.MAX_POLL_RECORDS_CONFIG] = 1
            it[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"
            it[ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG] = "false"
        }
        .let {
            KafkaConsumer(it)
        }

fun createMapper(): ObjectMapper =
        ObjectMapper().also {
            it.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }
