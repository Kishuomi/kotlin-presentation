package org.demo.kafka_interconnection


fun main(args: Array<String>) {
    val producer = createProducer()
    Runtime.getRuntime().addShutdownHook(Thread(producer::close))
    val topic = "kotlin_presentation_topic"

    val service = CallService(producer, topic)

    while (true) {
        val split = readLine()?.split(" ")!!
        if (split.size != 4) {
            continue
        }

        val id = split[0].toLongOrNull()
        val list = split.subList(1, split.size).map { it.toIntOrNull() }

        if (id == null || list.contains(null)) {
            continue
        }

        list.apply {
            Call(
                id = id,
                totalDayCalls = get(0)!!,
                totalEveCalls = get(1)!!,
                totalNightCalls = get(2)!!
            ).apply {
                service.sendCall(this)
            }
        }
    }
}