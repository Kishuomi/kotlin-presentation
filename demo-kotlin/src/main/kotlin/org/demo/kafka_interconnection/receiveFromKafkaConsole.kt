package org.demo.kafka_interconnection

import java.time.Duration

fun main(args: Array<String>) {

    val consumer = createConsumer().also {
        it.subscribe(listOf("kotlin_presentation_topic"))
    }
    Runtime.getRuntime().addShutdownHook(Thread(consumer::close))

    while (true) {
        consumer.poll(Duration.ofMillis(500L)).map {
            println("Key=${it.key()}")
            println("Value=${it.value()}")
        }
        consumer.commitSync()
    }
}