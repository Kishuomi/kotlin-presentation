package org.demo.kafka_interconnection

import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerRecord
import org.demo.dto.CallDto

class CallService(
    private val producer: Producer<Long, CallDto>,
    private val topic: String
) {

    fun sendCall(call: Call) {
        val dto = call.toDto()
        producer.send(ProducerRecord(topic, dto.id, dto))
    }

    private fun Call.toDto(): CallDto = CallDto(
        id = id,
        totalDayCalls = totalDayCalls,
        totalEveCalls = totalEveCalls,
        totalNightCalls = totalNightCalls,
        totalCalls = calculateTotalCalls()
    )

    private fun Call.calculateTotalCalls(): Int = totalDayCalls + totalEveCalls + totalNightCalls


    private fun calc(call: Call): Int {
        return call.totalDayCalls + call.totalEveCalls + call.totalNightCalls
    }

    private fun calc2(call: Call): Int = call.totalDayCalls + call.totalEveCalls + call.totalNightCalls


}