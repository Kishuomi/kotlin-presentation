package org.demo;

import org.demo.dto.JavaMessage;
import org.demo.dto.KotlinMessage;

import java.util.Collections;

public class MainJava {
    public static void main(String[] args) throws Exception {
        final JavaMessage<String, String> javaMessage = new JavaMessage<>("topic", "string");
        final KotlinMessage<String, String> kotlinMessage = new KotlinMessage<>("topic", 1, Collections.emptyMap(), "key", "value");
    }
}
