package org.demo.kafka_interconnection;

import org.apache.kafka.clients.consumer.Consumer;

import java.time.Duration;
import java.util.Collections;

public class ReceiveFromKafkaConsole {

  public static void main(String[] args) {
    final Consumer<Long, String> consumer = KafkaClientsFabric.createConsumer();
    consumer.subscribe(Collections.singletonList("kotlin_presentation_topic"));

    Runtime.getRuntime().addShutdownHook(new Thread(consumer::close));
    while (true) {
      consumer.poll(Duration.ofMillis(500L)).iterator().forEachRemaining(
          record -> {
            System.out.println("Key=" + record.key());
            System.out.println("Value=" + record.value());
          }
      );
    }
  }
}
