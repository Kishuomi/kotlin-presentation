package org.demo.kafka_interconnection;

import org.apache.kafka.clients.producer.Producer;
import org.demo.dto.CallDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.demo.kafka_interconnection.KafkaClientsFabric.createProducer;

public class SendToKafkaConsole {

  public static void main(String[] args) {
    final Producer<Long, CallDto> producer = createProducer();
    Runtime.getRuntime().addShutdownHook(new Thread(producer::close));
    final String topic = "kotlin_presentation_topic";

    final CallService service = new CallService(producer, topic);

    try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
      while (true) {
        final String[] split = reader.readLine().split(" ");
        if (split.length != 4) {
          continue;
        }
        final long id = Long.parseLong(split[0]);
        final int totalDayCalls = Integer.parseInt(split[1]);
        final int totalEveCalls = Integer.parseInt(split[2]);
        final int totalNightCalls = Integer.parseInt(split[3]);
        service.sendCall(new Call(id, totalDayCalls, totalEveCalls, totalNightCalls));
      }
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }
}
