package org.demo.kafka_interconnection;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.demo.dto.CallDto;

import javax.annotation.Nonnull;

public class CallService {
  private final Producer<Long, CallDto> producer;
  private final String topic;

  public CallService(@Nonnull final Producer<Long, CallDto> producer, @Nonnull final String topic) {
    this.producer = producer;
    this.topic = topic;
  }

  public void sendCall(@Nonnull final Call call) {
    final CallDto dto = toDto(call);
    producer.send(new ProducerRecord<>(topic, dto.getId(), dto));
  }

  @Nonnull
  private CallDto toDto(@Nonnull final Call call) {
    return new CallDto(
        call.getId(),
        call.getTotalDayCalls(),
        call.getTotalEveCalls(),
        call.getTotalNightCalls(),
        calculateTotalCalls(call)
    );
  }

  private int calculateTotalCalls(@Nonnull final Call call) {
    return call.getTotalDayCalls() + call.getTotalEveCalls() + call.getTotalNightCalls();
  }

}
