package org.demo.kafka_interconnection;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.demo.dto.CallDto;
import org.demo.serdes.JsonSerializer;

import java.util.Properties;

public final class KafkaClientsFabric {
  private KafkaClientsFabric() {
  }

  public static Producer<Long, CallDto> createProducer() {
    final Properties props = new Properties() {{
      put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    }};
    return new KafkaProducer<>(props, new LongSerializer(), new JsonSerializer<>(createMapper()));
  }
  
  public static Consumer<Long, String> createConsumer() {
    final Properties props = new Properties() {{
      put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
      put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
      put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
      put(ConsumerConfig.GROUP_ID_CONFIG, "java-presentation-group");
      put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);
      put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    }};
    return new KafkaConsumer<>(props);
  }

  private static ObjectMapper createMapper() {
    return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }
}
