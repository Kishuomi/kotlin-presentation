package org.demo.kafka_interconnection;

import lombok.Data;

@Data
public class Call {
  private final long id;
  private final int totalDayCalls;
  private final int totalEveCalls;
  private final int totalNightCalls;
}
