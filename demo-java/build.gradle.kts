plugins {
    java
}

dependencies {
    compile(project(":core-java"))
    compile(project(":core-kotlin"))

    annotationProcessor("org.projectlombok:lombok")
    implementation("org.projectlombok:lombok")
    implementation("com.google.code.findbugs:jsr305")
    implementation("org.apache.kafka:kafka-clients")
    implementation("com.fasterxml.jackson.core:jackson-core")
    implementation("com.fasterxml.jackson.core:jackson-databind")
}