rootProject.name = "kotlin-java-presentation"
include("core-java")
include("core-kotlin")
include("demo-java")
include("demo-kotlin")
